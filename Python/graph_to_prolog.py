import networkx
import random
import json
import sys
from networkx.readwrite import json_graph
from geopy.distance import great_circle
from osm_parser import download_osm, read_osm, highway_cat

def get_coordinates(G, node):
	return G.node[node]['lat'], G.node[node]['lon']

def print_node(G, node):
	nodelat, nodelon = get_coordinates(G, node)
	return "i({}, {}, {})".format(node, nodelat, nodelon)

def add_map_predicates(f, G):
	# aggiungi tutte le strade
	for n1,n2 in G.edges():
		f.write("strada({}, {}).\n".format(print_node(G, n1), print_node(G, n2)))

def add_position_predicates(f, G):
	reachable = False
	while (not reachable):
		# aggiungi la posizione dell'agente
		agent_node = G.nodes()[random.randint(0, len(G.nodes()) - 1)]
		# aggiungi il goal dell'agente
		goal_node = G.nodes()[random.randint(0, len(G.nodes()) - 1)]

		reachable = goal_node in networkx.descendants(G, agent_node)

	f.write("posizione({}).\n".format(print_node(G, agent_node)))
	f.write("goal_finale({}).\n".format(print_node(G, goal_node)))

def add_traffic(f, G):
	def tempo_percorrenza(n1, n2):
		# posso metterci fino al triplo del tempo
		traffico = 2 * random.random()

		# km al secondo
		speed_limit = 50.0 / (60.0 * 60.0)
		
		n1_coords = get_coordinates(G, n1)
		n2_coords = get_coordinates(G, n2)
		no_traffic_time = great_circle(n1_coords, n2_coords).kilometers / speed_limit

		return no_traffic_time * (1 + traffico)

	for n1, n2 in G.edges():
		# posso metterci fino al triplo del tempo
		f.write("percorrenza({}, {}, {}).\n".format(print_node(G, n1), print_node(G, n2), tempo_percorrenza(n1, n2)))	

if __name__ == '__main__':
	coordinates = [9.1941, 45.4520, 9.2077, 45.4635] if len(sys.argv) != 5 else sys.argv[1:]

	print("Importing from OpenStreetMap")
	G = read_osm(download_osm(*coordinates))

	print("Dumping to json for displayer program")
	dump = open("map_dump.json", "w")
	json.dump(json_graph.node_link_data(G), dump)
	dump.close()

	print("Dumping to pl for prolog program")
	f = open("map_export.pl", "w")
	print("Adding map predicates")
	add_map_predicates(f, G)

	print("Adding position predicates")
	add_position_predicates(f, G)

	print("Generating traffic")
	add_traffic(f, G)

	f.close()
	print("Map imported successfully")