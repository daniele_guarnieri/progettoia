:- module(mappa_spec, []).
:- use_module(library(is_a)).

type number.

type [i(number, number, number)] : incrocio.
% rappresenta un incrocio con le sue coordinate geospaziali
% i(I, Lat, Lon): I = indice del nodo in networkx, Lat e Lon = coordinate

pred distanza_linea_aria(incrocio, incrocio, number).
% distanza_linea_aria(+X, +Y, -D) det vero se e solo se la distanza fra X e Y è D.

pred strada(incrocio, incrocio).
% strada(+X, +Y) det vero se e solo se esiste una strada che va da X a Y
% strada(+X, -Y) nondet vero per tutti i nodi Y per cui esiste una strada da X a Y
% i numeri forniti sono gli indici dei nodi all'interno del grafo networkx

pred percorrenza(incrocio, incrocio, number).
% percorrenza(+X, +Y, -T) det vero se e solo se T è il tempo di percorrenza effettivo della strada che va da X ad Y

pred posizione(incrocio).
% rappresenta la posizione dell'agente

pred goal_finale(incrocio).
% rappresenta l'obiettivo finale dell'agente

%%% PREDICATI DI STIMA CONSERVATI FRA SESSIONI

pred stima_percorrenza(incrocio, incrocio, number).
% stima_percorrenza(+X, +Y, ?T): det vero se e solo se l'agente pensa di impiegare T per percorrere la strada da X a Y

pred aggiorna_stima_percorrenza(incrocio, incrocio, number).
% aggiorna_stima_percorrenza(+I0, +I1, +T) aggiorna la stima globale di percorrenza della strada I0->I1
% sapendo di aver impiegato T per percorrerla in questa sessione

pred stima_correlazione_traffico(incrocio, incrocio, incrocio, number).
% stima_correlazione_traffico(+X, +Y, +Z, -Alfa): det vero se e solo se l'agente pensa che 
% il traffico sulla strada che va da X a Y sia in correlazione col traffico sulla strada che
% va da Y a Z con un coefficiente Alfa

pred aggiorna_stima_correlazione_traffico(incrocio, incrocio, number, incrocio, number).
% aggiorna_stima_correlazione_traffico(+I0, +I1, +T0, +I2, +T1) aggiorna la stima globale di correlazione traffico sapendo di aver impiegato
% T0 per il tratto da I0 a I1 e T1 per il tratto da I1 a I2

%%% PREDICATI DINAMICI USATI PER RAGIONAMENTI NELLA SINGOLA SESSIONE

pred percorrenza_sessione(incrocio, incrocio, number).
% come stima_percorrenza