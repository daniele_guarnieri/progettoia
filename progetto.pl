﻿:- use_module(library(is_a)).
:- discontiguous(type(_)).
:- discontiguous(pred(_)).
:- discontiguous(gen(_)).


:- use_module(library(mr)).
:- consult(mappa).
:- consult(vai).

/*****  IMPORTO type, gen, pred DAI MODULI DI SPECIFICA ***/
:- use_module(vai_spec).
:- use_module(vai_if).
:- use_module(mappa_spec).

type T :- vai_spec:type(T).
type T :- vai_if:type(T).
type T :- mappa_spec:type(T).
pred P :- vai_spec:pred(P).
pred P :- vai_if:pred(P).
pred P :- mappa_spec:pred(P).

/***   IMPLEMENTO I TIPI DI vai_if ******/

type [new_data, old_data] : info_inizio.

gen [st(incrocio, incrocio)]: stato.

gen [
	% cerco un piano fra due incroci
	vado(incrocio, incrocio),

	% avanzo verso un incrocio adiacente
	avanzo(incrocio),

	% decido se ripianificare dopo aver percorso un tratto
	controllo_tempo(incrocio, incrocio),

	% termino l'esecuzione
	termino(evento)]: decisione.

gen [percorrenza_sessione(incrocio, incrocio, number)] : assumibile.

/***  A:  inizialiizzazioni  *****/

% importa la mappa esportata da python
:- consult(map_export).

pred clear_session_knowledge.
% comando qui introdotto per azzerare la conoscenza, che altrimenti
% resta acquisita ad ogni esecuzione e si accumula

clear_session_knowledge :-
	retractall(assunto(_)),
	retractall(conosce(_)).

pred clear_global_knowledge.
% azzero anche la conoscenza sulle stime acquisita nelle precedenti iterazioni
% utile per rieseguire l'agente sulle stesse condizioni

clear_global_knowledge :-
	retractall(stima_percorrenza(_, _, _)),
	retractall(stima_correlazione_traffico(_, _, _, _)).


pred initialize_global_knowledge.

initialize_global_knowledge :-
	SpeedLimit is 50 / (60*60),
	forall(strada(I1, I2), (
			distanza_linea_aria(I1, I2, D),
			S is D / SpeedLimit,
			assertz(stima_percorrenza(I1, I2, S)),
			forall(strada(I2, I3), (
					assertz(stima_correlazione_traffico(I1, I2, I3, 1))
				))
		)).

stato_iniziale(st(P0, G), old_data) :-
	strategy(astar),
	strategy(pota_chiusi),
	posizione(P0),
	goal_finale(G), 
	clear_session_knowledge.

stato_iniziale(st(P0, G), new_data) :-
	clear_global_knowledge,
	initialize_global_knowledge,
	stato_iniziale(st(P0, G), old_data).

/****  B: le decisioni *******/

% fine è specificato in vai_if; la decisione finale è termino(...)
fine(termino(_)).

% se sono arrivato termino
decidi(st(G, G), [eseguita(Dec)|_], termino(eseguita(Dec))) :- !.

% se sono all'inizio cerco un piano per andare al goal
decidi(st(P0, G), [inizio_storia(_)], vado(P0, G)).

% se il punto non è raggiungibile termino
decidi(_, [impossibile(vado(P, G))|_], termino(impossibile(vado(P,G)))).

% il fallimento dell'azione di controllo tmeporale comporta la ripianifica
decidi(st(P0, G), [fallita(vado(_P, G), [controllo_tempo(_, _)|_])|_], vado(P0, G)).

/****** C:   LE AZIONI  *******/

% azione è specificato in vai_if;  le azioni sono le seguenti:
azione(avanzo(_)).
azione(controllo_tempo(_, _)).

pred ripianifica.
:- dynamic(ripianifica/1).

% avanzo al prossimo incrocio
esegui_azione(st(P0, G), _Storia, avanzo(P1), st(P1, G)) :-
	strada(P0, P1),
	retract(posizione(P0)),
	assertz(posizione(P1)),
	decidi_se_ripianificare(P0, P1).

% controllo di averci messo più o meno quanto aspettavo, altrimenti fallisco l'azione
esegui_azione(st(P1, G), _Storia, controllo_tempo(_P0, P1), st(P1, G)) :-
	not(ripianifica).

% il controllo viene fatto durante l'azione di avanzamento perchè altrimenti
% il valore di stima utilizzato nella pianificazione viene modificato
% nella chiamata di aggiornamento della conoscenza
% il predicato ripianifica viene utilizzato per passare informazione
% dall'azione di avanzamento all'azione di controllo
decidi_se_ripianificare(I0, I1) :-
	Tolerance is 0.5,
	percorrenza(I0, I1, T),
	pensa(percorrenza_sessione(I0, I1, S), _),
	Tolerance >= abs(T - S) / S, !,
	retractall(ripianifica).

decidi_se_ripianificare(_I0, _I1) :-
	assertz(ripianifica).

/*****  D) La pianificazione delle decisioni non direttamente
 *         eseguibili come azioni                     ********/

% 1) ho deciso di cercare un piano per andare da P a G, lo cerco
piano(_ST, _Storia, vado(P,G), Piano) :-
	solve(P, Sol, =(G)),
	estrai_piano(Sol,Piano).

type [nc(punto,list(punto),number)]:nodo.
%  non stiamo a importare tutti i tipi di search_spec.pl
%  qui ci basta questo
pred estrai_piano(nodo, list(decisione)).
%  estrai_piano(+Sol, -Piano) det
%  Piano è la sequenza di avanzamenti che percorre la
%  sequenza di posizioni calcolata nella soluzione Sol
estrai_piano(nc(G,RevPath,_C), Piano) :-
	% faccio la reverse perchè il path è dal
	% nodo alla radice e quindi in senso inverso
	reverse([G|RevPath], Path),
	path2moves(Path, Piano).

pred path2moves(list(punto), list(decisione)).
path2moves([_H1, H2], [avanzo(H2)]) :- !.
path2moves([H1, H2|T], [avanzo(H2), controllo_tempo(H1, H2)|Moves]) :-
	path2moves([H2|T], Moves).


/*****  D1.  APPLICAZIONE DI A* e del ragionamento basato su
             assunzioni nella ricerca di un piano

	     A* è implementato nel modulo mr.pl e richiede
	     di implementare l'interfaccia search_if.pl

	     Il predicato pensa è implementato in vai.pl

*************************************************************/

%  implemento vicini, richiesto dall'interfaccia search_if
vicini(P, V) :-
	setof(P1, strada(P, P1), V), !; V=[].

%  implemento il costo, in base a quanto assume l'agente
%  nello stato di conoscenza attuale
costo(P, P2, C) :-
	pensa(percorrenza_sessione(P, P2, C), _).

% implemento l'euristica usando la distanza in linea d'aria
% implementazione per incroci
h(P,H) :-
	goal_finale(G),
	distanza_linea_aria(P, G, D),
	SpeedLimit is 50 / (60*60),
	H is D / SpeedLimit.

/******************  E)  PARTE DI RAGIONAMENTO  *******************/

%   aggiorna_conoscenza è specificata in vai_if.pl
%   avviene a fronte di un evento verificatosi

assumibile(percorrenza_sessione(_, _, _)).

% due assunzioni sono opposte se assumono due tempi diversi
contraria(percorrenza_sessione(I0, I1, T1), percorrenza_sessione(I0, I1, T2)) :- 
	T1 \= T2.

% se ancora non conosco un tempo di percorrenza per questa sessione, assumo che sia uguale a quello rilevato in precedenza
decide_se_assumere(percorrenza_sessione(I0, I1, T)) :-
	stima_percorrenza(I0, I1, T),
	assert(assunto(percorrenza_sessione(I0, I1, T))).

% cerco l'incrocio visitato prima di I0 per aggiornare la stima di correlazione traffico
pred cerca_provenienza(list(evento), incrocio, incrocio).
% cerca_provenienza(+H, +I0, -I1) semidet vero se e solo se nella storia c'è una transizione da I1 a I0
cerca_provenienza([transizione(_, avanzo(I0), _)|H], I0, I1) :- 
	cerca_provenienza_aux(H, I1).
cerca_provenienza([_|H], I0, I1) :-
	cerca_provenienza(H, I0, I1).

% ho trovato l'avanzamento in I0 -> cerco ora l'avanzamento subito precedente
% quello è il nodo da cui ho compiuto l'avanzamento verso I0
pred cerca_provenienza_aux(list(evento), incrocio).
cerca_provenienza_aux([transizione(_, avanzo(I), _)|_], I) :- !.
cerca_provenienza_aux([_|H], I) :-
	cerca_provenienza_aux(H, I).

%   Evento transizione(S1,A,S2,PL). E' stata eseguita la transizione
%   da S1 a S2, l'agente aggiorna in base al tempo impiegato per la percorrenza
%	la stima globale e in base alle correlazioni di traffico 
% 	anche le stime della sessione in corso
aggiorna_conoscenza(st(_P, G), H, transizione(st(P0, G), avanzo(P1), st(P1, G))) :-
	% se non è la prima strada che percorro, aggiornola stima globale della correlazione di traffico
	% non c'è il cut perchè dev'essere eseguito in aggiunta agli aggiornamenti sottostanti
	cerca_provenienza(H, P0, I),
	percorrenza(I, P0, T0),
	percorrenza(P0, P1, T1),
	aggiorna_stima_correlazione_traffico(I, P0, T0, P1, T1).

aggiorna_conoscenza(st(_P, G), _H, transizione(st(P0, G), avanzo(P1), st(P1, G))) :- !,
	% l'agente impara quanto ci ha messo a percorrere il tratto
	percorrenza(P0, P1, T),
	impara(percorrenza_sessione(P0, P1, T)),
	% aggiorno la stima gliobale
	aggiorna_stima_percorrenza(P0, P1, T),
	% aggiorno le stime di questa sessione in base alle correlazioni di traffico
	propaga_stima_traffico(P0, P1, T).

%  Per il cut, se arrivo qui non si ha nessuno dei casi precedenti,
%  non c'è nulla da imparare; l'agente non fa nulla
aggiorna_conoscenza(st(_P,_G), _H, _Evento).

% sapendo di aver impiegato T per percorrere il tratto da I0 a I1, propago l'informazione sulle strade uscenti da I1
% essendo un'informazione sul traffico, il cambiamento avviene solo nella base di conoscenza locale
% TODO: provare diverse ampiezze e coefficienti di propagazione
pred propaga_stima_traffico(incrocio, incrocio, number).
propaga_stima_traffico(I0, I1, T) :-
	propaga_stima_traffico(I0, I1, T, [0.8, 0.4, 0.2]).

% la lista rappresenta una lista di pesi in ampiezza: quanto devo considerare quest'informazione rispetto alla precedenza man mano che mi allontano
% dalla strada in cui ho rilevato effettivamente il traffico?
pred propaga_stima_traffico(incrocio, incrocio, number, list(number)).

% se ho esaurito i pesi con cui propagare mi fermo.
propaga_stima_traffico(_, _, _, []).

% altrimenti propago in ampiezza
% TODO: modo diverso per propagare? In particolare osservare comportamento in caso di collisioni
propaga_stima_traffico(I0, I1, T, [Coefficiente|Tail]) :-
	% traffico incontrato rispetto alle stime della sessione
	pensa(percorrenza_sessione(I0, I1, S), _),
	Traffico is (T - S) / S,
	% per ogni nodo adiacente ad I1 propago
	forall(strada(I1, I2),
		(
			pensa(percorrenza_sessione(I1, I2, OldS), _),
			stima_correlazione_traffico(I0, I1, I2, CT),
			NewS is (1 + Traffico * CT * Coefficiente) * OldS,
			aggiorna_stima_sessione(I1, I2, NewS, UsedS),
			% propago sui figli
			propaga_stima_traffico(I1, I2, UsedS, Tail)
		)).

pred aggiorna_stima_sessione(incrocio, incrocio, number, number).

aggiorna_stima_sessione(I0, I1, _NewS, UsedS) :-
	conosce(percorrenza_sessione(I0, I1, UsedS)), !.
aggiorna_stima_sessione(I0, I1, NewS, NewS) :-
	retractall(assunto(percorrenza_sessione(I0, I1, _))),
	assertz(assunto(percorrenza_sessione(I0, I1, NewS))).

mostra_stato_iniziale(_S0) :-
	mostra_stato.

mostra_transizione_stato(_S0, avanzo(_), _S1) :- !,
	mostra_stato.
mostra_transizione_stato(_, _, _).

pred mostra_stato.

mostra_stato :-
	posizione(i(_, ALat, ALon)),
	goal_finale(i(_, GLat, GLon)),
 	format(atom(CMD), "python2 Python/displayer.py ~f ~f ~f ~f", [ALat, ALon, GLat, GLon]),
 	shell(CMD).