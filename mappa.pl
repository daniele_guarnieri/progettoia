:- use_module(library(apply)).

pred deg2rad(number, number).

% presa da github swi-prolog/contrib.
distanza_linea_aria(i(_I1, Lat1deg, Lon1deg), i(_I2, Lat2deg, Lon2deg), D) :- 
	deg2rad(Lat1deg,Lat1),
    deg2rad(Lat2deg,Lat2),
   	deg2rad(Lon1deg,Lon1),
    deg2rad(Lon2deg,Lon2),
    DLat is Lat2 - Lat1,
    DLon is Lon2 - Lon1,
    A is (sin(DLat/2)**2) + cos(Lat1) * cos(Lat2) * (sin(DLon/2)**2),
    SqA is sqrt(A),
    OnemA is 1 - A,
    Sq1mA is sqrt(OnemA),
    C is 2 * atan(SqA,Sq1mA),
    D is 6371 * C.
	
deg2rad(Deg,Rad) :-
    Rad is (Deg * pi) / 180.

aggiorna_stima_percorrenza(P0, P1, T) :-
    Persistenza is 0.8,
    stima_percorrenza(P0, P1, S0),
    S1 is S0 * Persistenza + T * (1 - Persistenza),
    retract(stima_percorrenza(P0, P1, S0)),
    assertz(stima_percorrenza(P0, P1, S1)).

aggiorna_stima_correlazione_traffico(I0, I1, T0, I2, T1) :-
    % calcolo la correlazione del traffico incontrato
    stima_percorrenza(I0, I1, S0),
    Traffico0 is T0 - S0,
    stima_percorrenza(I1, I2, S1),
    Traffico1 is T1 - S1,
    CorrTraffico = Traffico1 / Traffico0,

    % aggiorno la stima
    Persistenza is 0.8,
    stima_correlazione_traffico(I0, I1, I2, StimaCorrTraffico),
    NuovaStima is StimaCorrTraffico * Persistenza + CorrTraffico * (1 - Persistenza),
    retract(stima_correlazione_traffico(I0, I1, I2, StimaCorrTraffico)),
    assertz(stima_correlazione_traffico(I0, I1, I2, NuovaStima)).

:- dynamic(posizione/1, goal_finale/1).
